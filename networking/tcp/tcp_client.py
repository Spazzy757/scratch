from socket import *

SERVER_PORT = 12000
SERVER_NAME = 'localhost'

def main():
    client_socket = socket(AF_INET, SOCK_STREAM)
    client_socket.connect((SERVER_NAME, SERVER_PORT))
    message = input('Input Lowercase sentence: ')
    client_socket.send(message.encode())
    modified_sentence = client_socket.recv(1024)
    print("From Server: ", modified_sentence.decode())
    client_socket.close()

if __name__=='__main__':
    main()
