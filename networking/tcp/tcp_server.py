from socket import *

SERVER_PORT = 12000

def main():
    server_socket = socket(AF_INET, SOCK_STREAM)
    server_socket.bind(('',SERVER_PORT))
    server_socket.listen(1)
    print("The server is ready to receive")
    while 1:
        connection_socket, addr = server_socket.accept()
        message = connection_socket.recv(1024)
        modified_message = message.upper()
        connection_socket.send(modified_message)
        connection_socket.close()

if __name__=='__main__':
    main()
