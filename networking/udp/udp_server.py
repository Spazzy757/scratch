from socket import *

SERVER_PORT = 12000

def main():
    server_socket = socket(AF_INET, SOCK_DGRAM)
    server_socket.bind(('127.0.0.1', SERVER_PORT))
    print("The server is ready to recieve")
    while 1:
        message, client_address = server_socket.recvfrom(2048)
        modified_message = message.upper()
        server_socket.sendto(modified_message, client_address)

if __name__=='__main__':
    main()
