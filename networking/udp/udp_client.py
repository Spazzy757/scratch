from socket import *

SERVER_NAME="localhost"
SERVER_PORT = 12000

def main():
    client_socket = socket(AF_INET, SOCK_DGRAM)
    message = input('Input lowercase sentence: ')
    client_socket.sendto(message.encode(), (SERVER_NAME, SERVER_PORT))
    modified_message, server_address = client_socket.recvfrom(2048)
    print(modified_message.decode())
    client_socket.close()

if __name__=='__main__':
    main()
